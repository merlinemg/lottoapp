from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from api import views


urlpatterns = [
    url(r'^createuser$', views.CreateUser.as_view(), name="createuser"),
    url(r'^login$', views.Login.as_view(), name='login'),
    url(r'^(?P<token>(\w|\-)+)$', views.Index.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/(?P<token>(\w|\-)+)$', views.ResourceDetail.as_view(), name='resource_detail'),
]