from django.contrib import admin

# Register your models here.
from api.models import UserProfile, Pick3, Calculations

admin.site.register(UserProfile)
admin.site.register(Pick3)
admin.site.register(Calculations)