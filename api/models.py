from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import uuid


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    token = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class Pick3(models.Model):
    midday_daily = models.CharField()
    evening_daily = models.CharField()
    draw_date = models.CharField(max_length=30)


class Calculations(models.Model):
    total_midday = models.IntegerField()
    total_evening = models.IntegerField()
    even_midday = models.IntegerField()
    even_evening = models.IntegerField()
    odd_midday = models.IntegerField()
    odd_evening = models.IntegerField()
    high_midday = models.IntegerField()
    high_evening = models.IntegerField()
    low_midday = models.IntegerField()
    low_evening = models.IntegerField()
    consecutive_midday = models.IntegerField()
    consecutive_evening = models.IntegerField()
