import sys, os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")
django.setup()
from api.models import UserProfile, Pick3, Calculations

from api.views import LotteryAPI


class SeedDatabase:
    def __init__(self):
        self.lottery_api = LotteryAPI()
        pick3_seed = self.seed_pick3()
        calculations_seed = self.seed_calculations()
        if (pick3_seed == True) and (calculations_seed == True):
            print("Success!")

    def seed_pick3(self):
        call = self.lottery_api.past_draws()
        for x in range(len(call)):
            Pick3.objects.create(midday_daily=call[x]['midday_daily'],
                                 evening_daily=call[x]['evening_daily'],
                                 draw_date=call[x]['draw_date'])
        return True

    def seed_calculations(self):
        call = self.lottery_api.past_draws()
        for x in range(len(call)):
            Calculations.objects.create(
                total_midday=self.total(call[x]['midday_daily']),
                total_evening=self.total(call[x]['evening_daily']),
                even_midday=self.even(call[x]['midday_daily']),
                even_evening=self.even(call[x]['evening_daily']),
                odd_midday=self.odd(call[x]['midday_daily']),
                odd_evening=self.odd(call[x]['evening_daily']),
                high_midday=self.high(call[x]['midday_daily']),
                high_evening=self.high(call[x]['evening_daily']),
                low_midday=self.low(call[x]['midday_daily']),
                low_evening=self.low(call[x]['evening_daily']),
                consecutive_midday=self.consecutive(call[x]['midday_daily']),
                consecutive_evening=self.consecutive(call[x]['evening_daily'])
            )

        return True

    def total(self, number):
        lista = []
        total = 0
        for x in number:
            lista.append(int(x))
        for y in lista:
            total += y
        return total

    def even(self, number):
        lista = []
        for x in number:
            lista.append(int(x))
        count = 0
        for i in lista:
            if i % 2 == 0:
                count += 1
        return count

    def odd(self, number):
        lista = []
        for x in number:
            lista.append(int(x))
        count = 0
        for i in lista:
            if i % 2 != 0:
                count += 1
        return count

    def high(self, number):
        lista = []
        for x in number:
            lista.append(int(x))
        right_format = tuple(lista)
        result = max(right_format)
        return result

    def low(self, number):
        lista = []
        for x in number:
            lista.append(int(x))
        right_format = tuple(lista)
        result = min(right_format)
        return result

    def consecutive(self, number):
        lista = []
        count = 0
        for x in number:
            lista.append(int(x))
        n = 0
        for i in lista:
            if n != (len(lista) - 1) and i + 1 == lista[n + 1]:
                n += 1
        if n == (len(lista) - 1):
            count += 1
        else:
            count += 0

        n = 0
        for i in lista:
            if n != (len(lista) - 1) and i - 1 == lista[n + 1]:
                n += 1
        if n == (len(lista) - 1):
            count += 1
        else:
            count += 0

        if count == 1:
            return True
        else:
            return False


            # def computer_mistake(self, number):
            # 	if len(number) == 3:
            # 		return number
            # 	elif len(number) == 2:
            # 		return '0' + number
            # 	elif len(number) == 1:
            # 		return '00' + number

    # def finder(self,input):
    #     output = []
    #     i = 0
    #     for(i = 0; i <= 999; i++):
    #         if (adder(i) === input
    #         output.append(i)
    #     return output
    #
    # def sum_digits(n):
    #     s = 0
    #     while n:
    #         s += n % 10
    #         n //= 10
    #     return s

test = SeedDatabase()
